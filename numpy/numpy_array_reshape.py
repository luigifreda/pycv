#!/usr/bin/env python3
import numpy as np

grid = np.arange(1, 10).reshape((3, 3))
print(grid)
# [[1 2 3]
#  [4 5 6]
#  [7 8 9]]

"""
Note that for this to work, the size of the initial array must match the size of the reshaped array. 
Where possible, the reshape method will use a no-copy view of the initial array, but with non-contiguous memory buffers this is not always the case.
"""
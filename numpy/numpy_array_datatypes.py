#!/usr/bin/env python3
import numpy as np

"""
Every numpy array is a grid of elements of the same type. Numpy provides a large set of numeric datatypes that you can use to construct arrays. 
Numpy tries to guess a datatype when you create an array, but functions that construct arrays usually also include an optional argument to 
explicitly specify the datatype. Here is an example:
"""

x = np.array([1, 2])   # Let numpy choose the datatype
print(x.dtype)         # Prints "int64"

x = np.array([1.0, 2.0])   # Let numpy choose the datatype
print(x.dtype)             # Prints "float64"

x = np.array([1, 2], dtype=np.int64)   # Force a particular datatype
print(x.dtype)                         # Prints "int64"


# conversion 
x = np.array([1, 2, 2.5])
print(x)
# array([ 1. ,  2. ,  2.5])
print(x.astype(int))   # Copy of the array, cast to a specified type.
# array([1, 2, 2])
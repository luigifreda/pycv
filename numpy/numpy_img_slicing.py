#!/usr/bin/env python3
import numpy as np 

delta = 2
start = np.array([[2,7],
 [5,3],
 [1,4]],dtype=np.intp)
print('start:',start)
end = start + delta
print('end:',end)
ranges = np.linspace(start,end,3,dtype=np.intp)[:,:].T 
print('ranges:',ranges)
elem = ranges[:,0]
print('elem:',elem)
img = np.arange(100).reshape(10,10)
print('\n img:',img)
print('\n img[elem[0],elem[1]]:',img[elem[0][:,np.newaxis],elem[1]])
#print('\n img[elem]:',img[elem])
import numpy as np
import cv2
from matplotlib import pyplot as plt


img = cv2.imread('data/butterfly.jpg',0)

# Create SURF object. You can specify params here or later.
# Here I set Hessian Threshold to 400
#surf = cv2.SURF(400)
surf = cv2.xfeatures2d.SURF_create(10000)

# In actual cases, it is better to have a value 300-500
#surf.hessianThreshold = 50000

# Find keypoints and descriptors directly
kp, des = surf.detectAndCompute(img,None)

print(len(kp))

img2 = cv2.drawKeypoints(img,kp,None,(255,0,0),4)

cv2.namedWindow('dst',cv2.WINDOW_KEEPRATIO)
cv2.imshow('dst',img2)
if cv2.waitKey(0) & 0xff == 27:
    cv2.destroyAllWindows()
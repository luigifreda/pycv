import numpy as np
import cv2
from matplotlib import pyplot as plt

img = cv2.imread('data/home.jpg')
gray= cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

#sift = cv2.SIFT()
sift = cv2.xfeatures2d.SIFT_create()
kp = sift.detect(gray,None)
"""
sift.detect() function finds the keypoint in the images. You can pass a mask if you want to search only a part of image. 
Each keypoint is a special structure which has many attributes like its (x,y) coordinates, size of the meaningful neighbourhood, 
angle which specifies its orientation, response that specifies strength of keypoints etc.
"""

#img=cv2.drawKeypoints(gray,kp)
cv2.drawKeypoints(gray,kp,img,flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
"""
OpenCV also provides cv2.drawKeyPoints() function which draws the small circles on the locations of keypoints. 
If you pass a flag, cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS to it, 
it will draw a circle with size of keypoint and it will even show its orientation. See below example.
"""

# Since you already found keypoints, you can call sift.compute() which computes the descriptors from the keypoints we have found. 
# If you didn’t find keypoints, directly find keypoints and descriptors in a single step with the function, sift.detectAndCompute().
kp,des = sift.compute(gray,kp)

cv2.imwrite('sift_keypoints.jpg',img)

cv2.namedWindow('dst',cv2.WINDOW_KEEPRATIO)
cv2.imshow('dst',img)
if cv2.waitKey(0) & 0xff == 27:
    cv2.destroyAllWindows()
import numpy as np
import cv2
import sys

def openVideo( video ):
    cap = cv2.VideoCapture(video)

    while(cap.isOpened()):
        ret, frame = cap.read()

        img = cv2.cvtColor(frame, cv2.cv2.IMREAD_COLOR)            

        cv2.imshow('frame',img)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    cap.release()
    cv2.destroyAllWindows()


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("%s <video.mp4>" % sys.argv[0])
        exit(-1)
    video = sys.argv[1]
    openVideo(video)
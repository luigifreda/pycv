import numpy as np
import cv2
from matplotlib import pyplot as plt

img = cv2.imread('data/objects.jpg',0)

# Initiate ORB detector
orb = cv2.ORB_create()

# find the keypoints with ORB
kp = orb.detect(img,None)

# compute the descriptors with ORB
kp, des = orb.compute(img, kp)

# draw only keypoints location,not size and orientation
img2 = cv2.drawKeypoints(img, kp, None, color=(0,255,0), flags=0)#cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

cv2.namedWindow('img2',cv2.WINDOW_KEEPRATIO)
cv2.imshow('img2',img2)

if cv2.waitKey(0) & 0xff == 27:
    cv2.destroyAllWindows()
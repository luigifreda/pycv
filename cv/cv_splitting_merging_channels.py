import numpy as np
import cv2

img = cv2.imread('data/messi5.jpg')

b,g,r = cv2.split(img)
img2 = cv2.merge((b,g,r))

cv2.imshow('image ',img)
cv2.imshow('b image ',b)
cv2.imshow('g image ',g)
cv2.imshow('r image ',r)
cv2.imshow('image2',img2)
cv2.waitKey(0)

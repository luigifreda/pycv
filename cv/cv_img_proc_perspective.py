import cv2
import numpy as np
from matplotlib import pyplot as plt

# https://docs.opencv.org/3.0-beta/doc/py_tutorials/py_imgproc/py_geometric_transformations/py_geometric_transformations.html#geometric-transformations

"""
Affine Transformation

In affine transformation, all parallel lines in the original image will still be parallel in the output image. 
To find the transformation matrix, we need three points from input image and their corresponding locations in output image. 
Then cv2.getAffineTransform will create a 2x3 matrix which is to be passed to cv2.warpAffine.
"""

img = cv2.imread('data/sudoku-original.jpg',cv2.IMREAD_COLOR)
rows,cols,ch = img.shape

pts1 = np.float32([[56,65],[368,52],[28,387],[389,390]])
pts2 = np.float32([[0,0],[300,0],[0,300],[300,300]])

M = cv2.getPerspectiveTransform(pts1,pts2)

dst = cv2.warpPerspective(img,M,(300,300))

plt.subplot(121),plt.imshow(img),plt.title('Input')
plt.subplot(122),plt.imshow(dst),plt.title('Output')
plt.show()
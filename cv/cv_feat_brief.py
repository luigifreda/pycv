import numpy as np
import cv2 
from matplotlib import pyplot as plt

img = cv2.imread('data/objects.jpg',0)

# Initiate FAST detector
star = cv2.xfeatures2d.StarDetector_create()

# Initiate BRIEF extractor
brief = cv2.xfeatures2d.BriefDescriptorExtractor_create()

# find the keypoints with STAR
kp = star.detect(img,None)

# compute the descriptors with BRIEF
kp, des = brief.compute(img, kp)
print( brief.descriptorSize() )
print( des.shape )

img2 = cv2.drawKeypoints(img, kp, None, color=(255,0,0),flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

cv2.namedWindow('img2',cv2.WINDOW_KEEPRATIO)
cv2.imshow('img2',img2)

if cv2.waitKey(0) & 0xff == 27:
    cv2.destroyAllWindows()
import cv2
import numpy as np
from matplotlib import pyplot as plt

# https://docs.opencv.org/3.0-beta/doc/py_tutorials/py_imgproc/py_filtering/py_filtering.html#filtering

"""

Goals
Learn to:
Blur the images with various low pass filters
Apply custom-made filters to images (2D convolution)
2D Convolution ( Image Filtering )
As in one-dimensional signals, images also can be filtered with various low-pass filters(LPF), high-pass filters(HPF) etc. 
LPF helps in removing noises, blurring the images etc. HPF filters helps in finding edges in the images.

OpenCV provides a function cv2.filter2D() to convolve a kernel with an image. As an example, we will try an averaging filter on an image. A 5x5 averaging filter kernel will look like below:

K =  \frac{1}{25} \begin{bmatrix} 1 & 1 & 1 & 1 & 1  \\ 1 & 1 & 1 & 1 & 1 \\ 1 & 1 & 1 & 1 & 1 \\ 1 & 1 & 1 & 1 & 1 \\ 1 & 1 & 1 & 1 & 1 \end{bmatrix}

Operation is like this: keep this kernel above a pixel, add all the 25 pixels below this kernel, 
take its average and replace the central pixel with the new average value. 
It continues this operation for all the pixels in the image. Try this code and check the result:

"""
img = cv2.imread('data/opencv_logo.png')

kernel = np.ones((5,5),np.float32)/25
dst = cv2.filter2D(img,-1,kernel)   # boder default: BORDER_REFLECT_101
                                    # when ddepth=-1, the output image will have the same depth as the source.

plt.subplot(121),plt.imshow(img),plt.title('Original')
plt.xticks([]), plt.yticks([])
plt.subplot(122),plt.imshow(dst),plt.title('Averaging')
plt.xticks([]), plt.yticks([])
plt.show()
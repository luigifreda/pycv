import numpy as np
import cv2

"""
Use the function cv2.imread() to read an image. The image should be in the working directory or a full path of image should be given.
Second argument is a flag which specifies the way image should be read.
- cv2.IMREAD_COLOR : Loads a color image. Any transparency of image will be neglected. It is the default flag.
- cv2.IMREAD_GRAYSCALE : Loads image in grayscale mode
- cv2.IMREAD_UNCHANGED : Loads image as such including alpha channel
"""


# Load an color image in grayscale
img_color = cv2.imread('data/messi5.jpg',cv2.IMREAD_COLOR)
print('img_color type: ', img_color.dtype) 

# convert to BGR 
image_color_bgr = img_color[..., [2,1,0]]

img_gray = cv2.imread('data/messi5.jpg',cv2.IMREAD_GRAYSCALE)
print('img_gray type: ', img_gray.dtype) 

cv2.namedWindow('image color',cv2.WINDOW_KEEPRATIO)  # in order to have the image resizable 
cv2.imshow('image color',img_color)

cv2.imshow('image image_color_bgr',image_color_bgr)
cv2.imshow('image gray',img_gray)

"""
cv2.waitKey() is a keyboard binding function. Its argument is the time in milliseconds. T
he function waits for specified milliseconds for any keyboard event. If you press any key in that time, the program continues. 
If 0 is passed, it waits indefinitely for a key stroke. 
It can also be set to detect specific key strokes like, if key a is pressed etc which we will discuss below.
"""
k= cv2.waitKey(0)
if k == 27:         # wait for ESC key to exit
    print('just exit')
elif k == ord('s'): # wait for 's' key to save and exit
    print('saving gray image')    
    cv2.imwrite('data/messigray.png',img_gray)

cv2.destroyAllWindows()
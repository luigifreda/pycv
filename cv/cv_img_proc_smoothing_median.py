import cv2
import numpy as np
from matplotlib import pyplot as plt

"""
3. Median Blurring
Here, the function cv2.medianBlur() takes median of all the pixels under kernel area and central element is replaced with this median value. 
This is highly effective against salt-and-pepper noise in the images. 
Interesting thing is that, in the above filters, central element is a newly calculated value which may be a pixel value in the image or a new value. 
But in median blurring, central element is always replaced by some pixel value in the image. It reduces the noise effectively. Its kernel size should be a positive odd integer.

In this demo, I added a 50% noise to our original image and applied median blur. Check the result:
"""

img = cv2.imread('data/salt_and_pepper.png')

median = cv2.medianBlur(img,5)

plt.subplot(121),plt.imshow(img),plt.title('Original')
plt.xticks([]), plt.yticks([])
plt.subplot(122),plt.imshow(median),plt.title('Median')
plt.xticks([]), plt.yticks([])
plt.show()